// Copyright (c) 2011 Chan Wai Shing
//           (c) 2016 Thomas Mitterfellner
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package syntaxhighlighter.brush;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Fortran brush.
 * @author Thomas Mitterfellner <thomas.mitterfellner@gmail.com>
 */
public class BrushFortran extends Brush {

  public BrushFortran() {
    super();

    String datatypes = "INTEGER REAL DOUBLE PRECISION CHARACTER LOGICAL COMPLEX";
    String keywords = "ASSIGN BACKSPACE BLOCK DATA CALL CLOSE COMMON CONTINUE DATA"
            + "DIMENSION DO ELSE ELSE IF END ENDFILE ENDIF ENTRY EQUIVALENCE EXTERNAL"
            + "FORMAT FUNCTION GOTO IF IMPLICIT INQUIRE INTRINSIC OPEN PARAMETER"
            + "PAUSE PRINT PROGRAM READ RETURN REWIND REWRITE SAVE STOP SUBROUTINE"
            + "THEN WRITE TYPE" // Fortran 77
            + "ALLOCATABLE ALLOCATE CASE CONTAINS CYCLE DEALLOCATE ELSEWHERE EXIT"
            + "INCLUDE INTERFACE INTENT MODULE NAMELIST NULLIFY ONLY OPERATOR"
            + "OPTIONAL POINTER PRIVATE PROCEDURE PUBLIC RECURSIVE RESULT SELECT"
            + "SEQUENCE TARGET USE WHILE WHERE" // Fortran 90
            + "ELEMENTAL FORALL PURE" // Fortran 95
            + "ABSTRACT ASSOCIATE ASYNCHRONOUS BIND CLASS DEFERRED ENUM ENUMERATOR"
            + "EXTENDS FINAL FLUSH GENERIC IMPORT NON_OVERRIDABLE NOPASS PASS"
            + "PROTECTED VALUE VOLATILE WAIT" // Fortran 2003
            + "BLOCK CODIMENSION DO CONCURRENT CONTIGUOUS CRITICAL ERROR STOP"
            + "SUBMODULE SYNC ALL SYNC IMAGES SYNC MEMORY LOCK UNLOCK"; // Fortran 2008
    String functions = "ABS ACHAR ACOS ACOSH ADJUSTL ADJUSTR AIMAG AINT ALL ALLOCATED"
            + "ANINT ANY ASIN ASINH ASSOCIATED ATAN ATAN2 ATANH ATOMIC_ADD ATOMIC_AND"
            + "ATOMIC_CAS ATOMIC_DEFINE ATOMIC_FETCH_ADD ATOMIC_FETCH_AND"
            + "ATOMIC_FETCH_OR ATOMIC_FETCH_XOR ATOMIC_OR ATOMIC_REF"
            + "ATOMIC_XOR BESSEL_J0 BESSEL_J1 BESSEL_JN BESSEL_Y0 BESSEL_Y1"
            + "BESSEL_YN BGE BGT BIT_SIZE BLE BLT BTEST C_ASSOCIATED"
            + "C_FUNLOC C_F_PROCPOINTER C_F_POINTER C_LOC C_SIZEOF CEILING CHAR CMPLX"
            + "CO_BROADCAST CO_MAX CO_MIN CO_REDUCE CO_SUM COMMAND_ARGUMENT_COUNT"
            + "COMPILER_OPTIONS COMPILER_VERSION CONJG COS COSH COUNT CPU_TIME CSHIFT"
            + "DATE_AND_TIME DBLE DIGITS DIM DOT_PRODUCT DPROD DSHIFTL DSHIFTR EOSHIFT"
            + "EPSILON ERF ERFC ERFC_SCALED EVENT_QUERY EXECUTE_COMMAND_LINE EXP"
            + "EXPONENT EXTENDS_TYPE_OF FLOAT FLOOR FRACTION GAMMA GET_COMMAND"
            + "GET_COMMAND_ARGUMENT GET_ENVIRONMENT_VARIABLE HUGE HYPOT IACHAR IALL"
            + "IAND IANY IBCLR IBITS IBSET ICHAR IEOR IMAGE_INDEX INDEX INT IOR"
            + "IPARITY IS_IOSTAT_END IS_IOSTAT_EOR ISHFT ISHFTC KIND LBOUND LCOBOUND"
            + "LEADZ LEN LEN_TRIM LGE LGT LLE LLT LOG LOG10 LOG_GAMMA LOGICAL MASKL"
            + "MASKR MATMUL MAX MAXEXPONENT MAXLOC MAXVAL MERGE MERGE_BITS MIN"
            + "MINEXPONENT MINLOC MINVAL MOD MODULO MOVE_ALLOC MVBITS NEAREST NEW_LINE"
            + "NINT NOT NORM2 NULL NUM_IMAGES PACK PARITY POPCNT POPPAR PRECISION"
            + "PRESENT PRODUCT RADIX RANDOM_NUMBER RANDOM_SEED RANGE RANK REAL"
            + "REPEAT RESHAPE RRSPACING SAME_TYPE_AS SCALE SCAN SELECTED_CHAR_KIND"
            + "SELECTED_INT_KIND SELECTED_REAL_KIND SET_EXPONENT SHAPE SHIFTA SHIFTL"
            + "SHIFTR SIGN SIN SINH SIZE SNGL SPACING SPREAD SQRT STORAGE_SIZE SUM"
            + "SYSTEM_CLOCK TAN TANH THIS_IMAGE TINY TRAILZ TRANSFER TRANSPOSE TRIM"
            + "UBOUND UCOBOUND UNPACK VERIFY";
    String special = "TRUE FALSE EQ NE LT GT LE GE AND OR NOT EQV NEQV";

    List<RegExpRule> _regExpRuleList = new ArrayList<RegExpRule>();
    _regExpRuleList.add(new RegExpRule("!.*$|^[cC].*$", Pattern.MULTILINE, "comments")); // comments
    _regExpRuleList.add(new RegExpRule(RegExpRule.doubleQuotedString, "string")); // strings
    _regExpRuleList.add(new RegExpRule(RegExpRule.singleQuotedString, "string")); // strings
    _regExpRuleList.add(new RegExpRule(getKeywords(datatypes), Pattern.MULTILINE | Pattern.CASE_INSENSITIVE, "keyword"));
    _regExpRuleList.add(new RegExpRule(getKeywords(functions), Pattern.MULTILINE | Pattern.CASE_INSENSITIVE, "functions"));
    _regExpRuleList.add(new RegExpRule(getKeywords(keywords), Pattern.MULTILINE | Pattern.CASE_INSENSITIVE, "keyword"));
    _regExpRuleList.add(new RegExpRule("(-?\\.?)(\\b(\\d*\\.?\\d+|\\d+\\.?\\d*)([de][+-]?\\d+)?)\\b\\.?", Pattern.CASE_INSENSITIVE, "color2")); // numbers
    _regExpRuleList.add(new RegExpRule("\\+|\\-|\\*|\\/|\\%|=|==|\\*\\*", Pattern.MULTILINE, "keyword"));
    _regExpRuleList.add(new RegExpRule(getKeywords(special), Pattern.MULTILINE | Pattern.CASE_INSENSITIVE, "color1"));
    
    setRegExpRuleList(_regExpRuleList);

    setCommonFileExtensionList(Arrays.asList("f", "F", "f90"));
  }
}

